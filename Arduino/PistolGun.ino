#include <SoftwareSerial.h>
#include <Servo.h>

const int RX_PIN = 3;
const int TX_PIN = 2;
SoftwareSerial BT(RX_PIN, TX_PIN);
char commandChar;
Servo myservo;

boolean ledon = false;
String string;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  BT.begin(9600);
  pinMode(5, OUTPUT);
  myservo.attach(9);
}

void loop() {
  // put your main code here, to run repeatedly:
      if (BT.available() > 0) 
    {string = "";}
    
        while(BT.available() > 0)
    {
      commandChar = ((byte)BT.read());
      
      if(commandChar == ':')
      {
        break;
      }
      
      else
      {
        string += commandChar;
      }
      
      delay(1);
    }
    
        if(string == "TO")
    {
        ledOn();
        ledon = true;
    }
    
    if(string =="TF")
    {
        ledOff();
        ledon = false;
        Serial.println(string);
    }
    
    if ((string.toInt()>=0)&&(string.toInt()<=255))
    {
      if (ledon==true)
      {
        analogWrite(5, string.toInt());
        Serial.println(string);
        delay(10);
      }
    }
    
//  if(BT.available())
//  {
//    commandChar = BT.read();
//    switch(commandChar)
//    {
//      case '*':
//      BT.print(random(1000)+"#");
//      break;
//    }
//  }
}

void ledOn()
   {
      analogWrite(5, 255);
      delay(10);
      myservo.write(180);
    }
 
 void ledOff()
 {
      analogWrite(5, 0);
      delay(10);      
      myservo.write(90);
 }

void sendBT(double lux){
  if(BT.available()){
    BT.print("#");
    BT.print((int)lux);
    BT.print("~");
  }
  delay (10);
}

void readBT(){
  if(BT.available()){
    char i =(char)BT.read();
    Serial.println(i);
  }
}
