/* Copyright 2016 Michael Sladoje and Mike Schälchli. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

package xyz.imago.gunsafety.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import xyz.imago.gunsafety.ConnectionService.JSONParser;
import xyz.imago.gunsafety.Helpers.FileHelper;
import xyz.imago.gunsafety.Helpers.MatOperation;
import xyz.imago.gunsafety.PreProcessor.PreProcessorFactory;
import xyz.imago.gunsafety.R;
import xyz.imago.gunsafety.Recognition.Recognition;
import xyz.imago.gunsafety.Recognition.RecognitionFactory;

public class RecognitionActivity extends Activity implements CameraBridgeViewBase.CvCameraViewListener2 {
    private JavaCameraView mRecognitionView;
    private static final String TAG = "Recognition";
    private FileHelper fh;
    private Recognition rec;
    private PreProcessorFactory ppF;
    private ProgressBar progressBar;
    private ProgressDialog progress;
    private boolean front_camera;

    //For Bluetooth
    String address = null;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    private boolean isBtConnected = false;
    //SPP UUID. Look for it
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private int STABLE = 0;
    private int DANGER = 1;

    //For Internet
    private String humanName = "";
    JSONArray humans = null;
    ArrayList<HashMap<String, String>> humanList;
    private String urlServer;
    private String urlAllHumans;
    private boolean isHumanDataAvailable = false;
    // Creating JSON Parser object
    JSONParser jParser = new JSONParser();

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_HUMANS = "humans";
    private static final String TAG_ID = "idx";
    private static final String TAG_NAME = "name";
    private static final String TAG_STATUS = "status";
    private String humanNameCurrent = "";

    private Handler handlerDetectHuman = new Handler();


    static {
        if (!OpenCVLoader.initDebug()) {
            // Handle initialization error
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.recognition_layout);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        Intent intentBefore = getIntent();
        address = intentBefore.getStringExtra(DeviceListActivity.EXTRA_ADDRESS);

        fh = new FileHelper();
        File folder = new File(fh.getFolderPath());
        if (folder.mkdir() || folder.isDirectory()) {
            Log.i(TAG, "New directory for photos created");
        } else {
            Log.i(TAG, "Photos directory already existing");
        }

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        urlServer = sharedPref.getString("url_list_human", "http://192.168.144.1");
        urlAllHumans = urlServer + "/gunsafety/get_all_humans.php";

        // Hashmap for ListView
        humanList = new ArrayList<>();

        // Use camera which is selected in settings
        front_camera = sharedPref.getBoolean("key_front_camera", true);
        mRecognitionView = (JavaCameraView) findViewById(R.id.RecognitionView);
        if (front_camera) {
            mRecognitionView.setCameraIndex(1);
        } else {
            mRecognitionView.setCameraIndex(-1);
        }
        mRecognitionView.setVisibility(SurfaceView.VISIBLE);
        mRecognitionView.setCvCameraViewListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mRecognitionView != null)
            mRecognitionView.disableView();
//        handlerDetectHuman.removeCallbacks(runnable);
    }

    public void onDestroy() {
        super.onDestroy();
        if (mRecognitionView != null)
            mRecognitionView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
    }

    public void onCameraViewStopped() {
    }

    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Mat imgRgba = inputFrame.rgba();
        Mat img = new Mat();
        imgRgba.copyTo(img);
        List<Mat> images = ppF.getProcessedImage(img);
        Rect[] faces = ppF.getFacesForRecognition();
        // Selfie / Mirror mode
        if (front_camera) {
            Core.flip(imgRgba, imgRgba, 1);
        }
        if (images == null || images.size() == 0 || faces == null || faces.length == 0 || !(images.size() == faces.length)) {
            // skip
            return imgRgba;
        } else {
            faces = MatOperation.rotateFaces(imgRgba, faces, ppF.getAngleForRecognition());
            for (int i = 0; i < faces.length; i++) {
                humanName = rec.recognize(images.get(i), "");
                Scalar rectColor;
                if (scanHumanData()) {
                    rectColor = new Scalar(255, 0, 0, 255);
                } else
                    rectColor = new Scalar(0, 255, 0, 255);
                MatOperation.drawRectangleAndLabelOnPreview(imgRgba, faces[i], humanName, front_camera, rectColor);
            }
            return imgRgba;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int N = Integer.valueOf(sharedPref.getString("key_N", "25"));
        ppF = new PreProcessorFactory(getApplicationContext(), N);

        new PrepareScanner().execute();
//        //Task to get data from JSON
//        new GetHumansData().execute();
//
//        //Task to connect to Arduino
//        new ControlArduino().execute();

        final android.os.Handler handler = new android.os.Handler(Looper.getMainLooper());
        Thread t = new Thread(new Runnable() {
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                });
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String algorithm = sharedPref.getString("key_classification_method", getResources().getString(R.string.eigenfaces));
                rec = RecognitionFactory.getRecognitionAlgorithm(getApplicationContext(), Recognition.RECOGNITION, algorithm);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        });

        t.start();

        // Wait until Eigenfaces loading thread has finished
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        handlerDetectHuman.post(runnable);
        mRecognitionView.enableView();
    }

    /**
     * Async task class to prepare scanner
     */
    private class PrepareScanner extends AsyncTask<Void, Void, Void> {
        private boolean ConnectBluetoothSuccess = true; //if it's here, it's almost connected

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(RecognitionActivity.this, "Preparing The Scanner", "Please wait...");  //show a progress dialog
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            //Connect Bluetooth
            try {
                if (btSocket == null || !isBtConnected) {
                    myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice bluetoothDevice = myBluetooth.getRemoteDevice(address);//connects to the device's address and checks if it's available
                    btSocket = bluetoothDevice.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();//start connection
                }
            } catch (IOException e) {
                ConnectBluetoothSuccess = false;//if the try failed, you can check the exception here
            }

            //Connect to Server
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONObject json = jParser.makeHttpRequest(urlAllHumans, "GET", params);
            Log.d("All Products: ", json.toString());

            try {
                int success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    humans = json.getJSONArray(TAG_HUMANS);
                    for (int i = 0; i < humans.length(); i++) {
                        JSONObject c = humans.getJSONObject(i);
                        String id = c.getString(TAG_ID);
                        String name = c.getString(TAG_NAME);
                        String stats = c.getString(TAG_STATUS);

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_ID, id);
                        map.put(TAG_NAME, name);
                        map.put(TAG_STATUS, stats);
                        humanList.add(map);
                    }
                    isHumanDataAvailable = true;
                } else {
                    isHumanDataAvailable = false;
                }
            } catch (JSONException e) {
                isHumanDataAvailable = false;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (!ConnectBluetoothSuccess) {
                msg("Connection Failed. Is it a SPP Bluetooth? Try again.");
                finish();
            } else {
                msg("Connected.");
                isBtConnected = true;
            }
            if (!isHumanDataAvailable) {
                msg("Error Retrieving Data");
                finish();
            } else
                msg("Success retrieving data");
            progress.dismiss();
        }
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class GetHumansData extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONObject json = jParser.makeHttpRequest(urlAllHumans, "GET", params);
            Log.d("All Products: ", json.toString());
            try {
                int success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    humans = json.getJSONArray(TAG_HUMANS);
                    for (int i = 0; i < humans.length(); i++) {
                        JSONObject c = humans.getJSONObject(i);

                        String id = c.getString(TAG_ID);
                        String name = c.getString(TAG_NAME);
                        String stats = c.getString(TAG_STATUS);

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_ID, id);
                        map.put(TAG_NAME, name);
                        map.put(TAG_STATUS, stats);

                        humanList.add(map);
                    }
                    isHumanDataAvailable = true;
                } else {
                    // no products found, error connect

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    //Runnable for detecting face recognition
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            handlerDetectHuman.postDelayed(runnable, 10000);
            //Task to get data from JSON
            new GetHumansData().execute();
        }
    };

    private boolean scanHumanData() {
        boolean isDanger = false;
        if (isBtConnected && isHumanDataAvailable) {
            if (humanName.isEmpty()) {
            } else {
                int idName = 0;
                int idDanger = 0;
                HashMap<String, String> map = new HashMap<String, String>();
                for (int i = 0; i < humans.length(); i++) {
                    map = humanList.get(i);
                    for (int j = 0; j < 3; j++) {
                        if (map.containsValue(humanName))
                            idName = i;
                        if (map.containsValue("Danger") && map.containsValue(humanName))
                            idDanger = i;
                    }
                }
                if (!humanNameCurrent.equals(humanName)) {
                    if (idName == idDanger) {
                        isDanger = true;
                        ControlLock(DANGER);
                    } else {
                        isDanger = false;
                        ControlLock(STABLE);
                    }
                    humanNameCurrent = humanName;
                }
            }
        }
        return isDanger;
    }

    private void ControlLock(int condition) {
        if (btSocket != null) {
            if (condition == STABLE) {
                try {
                    btSocket.getOutputStream().write("TO".toString().getBytes());
                } catch (IOException e) {
                    msg("Error");
                }
            } else if (condition == DANGER) {
                try {
                    btSocket.getOutputStream().write("TF".toString().getBytes());
                } catch (IOException e) {
                    msg("Error");
                }
            }
        }
    }

    private void msg(String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
    }
}
